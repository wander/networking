## Networking

Core library for network related tasks. Higher level networking prefabs
and systems can be build on top of this one. This serves as a utility package.


### Dependencies
-  Utils


### Note
- For network discovery work correctly within the editor, ParrelSync package must be installed. That way, multiple unity editors can be used to test networking.
